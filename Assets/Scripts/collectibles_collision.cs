﻿using UnityEngine;
using System.Collections;

public class collectibles_collision : MonoBehaviour {

	private UI UI;
	private GameObject Game_manager_object;


	void Start ()
	{
		Game_manager_object = GameObject.FindWithTag ("GameController");
		UI = Game_manager_object.GetComponent <UI> ();
	}

	void OnTriggerEnter2D (Collider2D other){
		if (other.gameObject.tag == "Player") {
			Destroy (gameObject);
			UI.Add_score (1);
		}
	}

}