﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class UI : MonoBehaviour {

	public Text score_text;
	public AudioClip point;
	public AudioClip die;

	private int score;
	private int high_score;
	private Spawning Spawn;
	private int count;
	private Scene Menu;
	private AudioSource source;

	private string one = "CgkI3qz-qt0BEAIQAw";
	private string ten = "CgkI3qz-qt0BEAIQBA";
	private string fifty = "CgkI3qz-qt0BEAIQAg";
	private string hundred = "CgkI3qz-qt0BEAIQBg";
	private string twohundred = "CgkI3qz-qt0BEAIQBQ";
	private string leaderboard = "CgkI3qz-qt0BEAIQAQ";

	void Awake(){
		source = GetComponent<AudioSource> ();
	}

	void Start () {
		Screen.orientation = ScreenOrientation.Portrait;
		Screen.autorotateToPortrait = true;
		Screen.autorotateToPortraitUpsideDown = true;
		Screen.autorotateToLandscapeRight = false;
		Screen.autorotateToLandscapeLeft = false;
		Screen.orientation = ScreenOrientation.AutoRotation;

		score = 0;
		high_score = PlayerPrefs.GetInt ("High Score");
		UpdateScore ();
		Spawn = GetComponent<Spawning>();
		count = 0;
	}

	public IEnumerator wait_seconds (float x) {
		yield return new WaitForSeconds (x);
		to_menu ();
	}

	public void tothe_menu(){
		source.PlayOneShot (die, 2f);
		if (score > high_score) {
			high_score = score;
			PlayerPrefs.SetInt ("High Score", high_score);
			Social.ReportScore (high_score, leaderboard, (bool success) => {
			});
			if (high_score >= 1) {
				Social.ReportProgress (one, 100.0f, (bool success) => {
				});
			}
			if (high_score >= 10) {
				Social.ReportProgress (ten, 100.0f, (bool success) => {
				});
			} 
			if (high_score >= 50) {
				Social.ReportProgress (fifty, 100.0f, (bool success) => {
				});
			} 
			if (high_score >= 100) {
				Social.ReportProgress (hundred, 100.0f, (bool success) => {
				});
			} 
			if (high_score >= 200) {
				Social.ReportProgress (twohundred, 100.0f, (bool success) => {
				});
			}
		}
		StartCoroutine(wait_seconds(2f));
	}

	public void to_menu(){
		SceneManager.LoadScene ("Menu");
	}

	public void Add_score (int newScoreValue)
	{
		score += newScoreValue;
		source.PlayOneShot (point);
		UpdateScore ();
		count += 1;
		if (count == 10) {
			Spawn.spawnreset ();
			count = 0;
		}
	}

	void UpdateScore ()
	{
		score_text.text = score.ToString ();

	}

}
