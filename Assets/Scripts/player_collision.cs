﻿using UnityEngine;
using System.Collections;

public class player_collision : MonoBehaviour {

	private UI UI;
	private GameObject Game_manager_object;

	void Start ()
	{
		Game_manager_object = GameObject.FindWithTag ("GameController");
		UI = Game_manager_object.GetComponent <UI> ();
	}

	void OnTriggerEnter2D (Collider2D other){
		
		if (other.gameObject.tag == "Enemy"){
			Debug.Log ("oh my, I'm dead!");
			UI.tothe_menu ();
			gameObject.SetActive (false);
		}
	}
}
