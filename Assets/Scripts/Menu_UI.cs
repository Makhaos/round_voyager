﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class Menu_UI : MonoBehaviour {

	public AudioClip audioPlay;
	public Text high_score_text;

	private int high_score;
	private AudioSource source;
	private string theleaderboard = "CgkI3qz-qt0BEAIQAQ";

	void Awake(){
		source = GetComponent<AudioSource> ();
	}

	void Start(){
		PlayGamesPlatform.Activate ();
		Social.localUser.Authenticate((bool success) => {
			//success
		});

		Screen.orientation = ScreenOrientation.Portrait;
		Screen.autorotateToPortrait = true;
		Screen.autorotateToPortraitUpsideDown = true;
		Screen.autorotateToLandscapeRight = false;
		Screen.autorotateToLandscapeLeft = false;
		Screen.orientation = ScreenOrientation.AutoRotation;

		high_score = PlayerPrefs.GetInt ("High Score");
		high_score_text.text = high_score.ToString ();
	}

	public void Play (string x) {
		source.PlayOneShot (audioPlay);
		SceneManager.LoadScene (x);
	}

	public void Leaderboard (){
		PlayGamesPlatform.Instance.ShowLeaderboardUI(theleaderboard);
	}

	public void Achievements (){
		Social.ShowAchievementsUI ();
	}

	void Update() {
		if (Input.GetKeyDown (KeyCode.A)) {
			
			Application.CaptureScreenshot ("Gimp/Screenshot.png");
		}
	}
}
