﻿using UnityEngine;
using System.Collections;

public class Spawning : MonoBehaviour {

	public GameObject Enemy;
	public GameObject Destroyer;
	public GameObject Friend;

	public static float positionf_x;
	public static float speed;
	public static float starting_position;

	private GameObject spawned_enemy;
	private float spawntime;
	private float[] scale_x = {2f, 2.2f, 2.5f, 2.7f, 3f, 3.8f, 4f, 4.2f, 4.5f, 4.8f, 5f};
	private float[] scale_y = {0.1f};
	private int index_x;
	private float position_x = 2.2f;
	private int index_position;
	private int prob;

	public void Start_spawning () {//Initialized by player_movement
		Instantiate (Destroyer);
		speed = 5f;
		starting_position = 8f;
		positionf_x = 2.2f;
		spawntime = 0.7f;
		StartCoroutine (spawnlevelup());
		Invoke ("Spawn", 0);

	}
	
	void Spawn () {
		prob = Random.Range(0,100);

		//Spawning
		if (prob <= 70) {
			spawned_enemy = Instantiate (Enemy);
		} else if (prob > 70) {
			spawned_enemy = Instantiate (Friend);
		}

		dimensions ();
		position ();

		//Recursion
		Invoke ("Spawn", spawntime);
	}

	void dimensions (){
		if (spawned_enemy.tag == "Enemy" || spawned_enemy.tag == "Friend") {
			index_x = Random.Range (0, scale_x.Length);
			spawned_enemy.transform.localScale = new Vector2 (scale_x [index_x], scale_y [0]);
		}
	}

	void position (){
		position_x = -position_x;	
		positionf_x = position_x;
		spawned_enemy.transform.position = new Vector2 (positionf_x, starting_position);
	}

	public void spawnreset(){
		spawntime = 0.7f;
		if (speed < 6f) {
			speed += 0.2f;
		} 
	} 

	public IEnumerator spawnlevelup() {
		while (enabled) {
			
			Debug.Log(spawntime);
			yield return new WaitForSeconds (5f);
			if (spawntime > 0.5f) {
				spawntime -= 0.1f;
			}
		}
			
	}

}