﻿using UnityEngine;
using System.Collections;

public class player_movement : MonoBehaviour {

	public float speed;
	public Rigidbody2D rb;
	public AudioClip move;

	private GameObject gm;
	private bool mov;
	private Vector3 direction;
	private float acc;
	private AudioSource source;
	private float vol = 0.5f;
	private Spawning Spawning;

	void Awake (){
		source = GetComponent<AudioSource>();
	}

	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		gm = GameObject.Find ("Game_manager");
		Spawning = gm.GetComponent<Spawning>(); 
		speed = 25f;
		acc = 1.03f;
		direction = Vector3.back;
		mov = false;
	}

	//for rigidbody movement
	void FixedUpdate () {

		if (mov == true) 
		{
			movement ();
		}
			
	}

	//using update to work with GetKeyDown and GetKeyUp
	void Update () {

		trigger ();
//		if (Input.GetKeyDown (KeyCode.A)) {
//
//			Application.CaptureScreenshot("Gimp/ingame.png");
//		}
	}
		
	void movement () {
		
		if (speed < 75f) {
			speed = speed * acc;
		} else {
			speed = 75f;
		}
		transform.RotateAround (gm.transform.position, direction, speed * Time.deltaTime);
	}

	//stop player movement with key or touch press
	void trigger () {

		#if UNITY_STANDALONE || UNITY_EDITOR

		if (mov == false && Input.GetKeyDown(KeyCode.S)){
			mov = true;
			source.PlayOneShot(move, vol);
			Spawning.Start_spawning();
		}
		if (mov == true && Input.GetKeyDown (KeyCode.S)) {
			source.PlayOneShot(move, vol);
			speed = 35f;
			direction = -direction;

		}
		#elif UNITY_ANDROID || UNITY_IOS 

		if(Input.touchCount > 0)	{
		Touch myTouch = Input.GetTouch(0);

		if (mov == false && myTouch.phase == TouchPhase.Began)
		{
		mov = true;
		source.PlayOneShot(move, vol);
		Spawning.Start_spawning();
		}
			
		if (mov == true && myTouch.phase == TouchPhase.Began)
			{
				source.PlayOneShot(move, vol);
				speed = 35f;
				direction = -direction;
			}
		}

		#endif
			
	}
}