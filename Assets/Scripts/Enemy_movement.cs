﻿using UnityEngine;
using System.Collections;

public class Enemy_movement : MonoBehaviour {

	private float start_position = Spawning.starting_position;
	private float speed = Spawning.speed;
	private float y;
	private Vector2 position;
	private float x = Spawning.positionf_x;

	// Use this for initialization
	void Start () {
		y = start_position;
	}

	// Update is called once per frame
	void Update () {
		y += speed * -Time.deltaTime;
		position = new Vector2 (x, y);
		transform.position = position;		
	}

	void OnTriggerEnter2D (Collider2D other){
		if (other.gameObject.tag == "Destroyer"){
			Destroy (gameObject);
		}
	}
}
